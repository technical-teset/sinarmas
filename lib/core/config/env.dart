import 'package:sinarmas/core/config/env_fields.dart';

import 'env_dev.dart';

abstract class Env implements EnvFields {
  /// NOTE: This is here just as an example!
  ///
  /// In a Flutter app you would normally import this like so
  /// import 'package:flutter/foundation.dart';
  static const kDebugMode = true;

  factory Env() => _instance;

  static final Env _instance = _mapInstance();

  static Env _mapInstance() {
    return DevEnv();
  }
}
