import 'package:envied/envied.dart';
import 'package:sinarmas/core/config/config.dart';

part 'env_dev.g.dart';

@Envied(name: 'Env', path: '.env.prod')
class DevEnv implements Env, EnvFields {
  DevEnv();

  @override
  @EnviedField(varName: 'BASEURL')
  final String baseUrl = _Env.baseUrl;
}
