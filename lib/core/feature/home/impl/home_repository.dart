import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sinarmas/core/api/meals/meals_api.dart';
import 'package:sinarmas/core/client/client.dart';
import 'package:sinarmas/core/errors/failures.dart';
import 'package:sinarmas/core/feature/home/domain/list_meals.dart';

@injectable
class MealsRepository {
  final MealsApi _api;

  MealsRepository({
    required MealsApi api,
  }) : _api = api;

  Future<Either<Failure, ListCategoriesData>> getMealsList() async {
    final call = await apiCall(
      _api.getMealsList(),
    );

    return call.fold(left, (r) {
      return right(ListCategoriesData.fromResponse(r));
    });
  }

  Future<Either<Failure, ListCategoriesData>> getMealsDetail(
      {required String i}) async {
    final call = await apiCall(
      _api.getMealsDetail(i: i),
    );

    return call.fold(left, (r) {
      return right(ListCategoriesData.fromResponse(r));
    });
  }
}
