import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:sinarmas/core/errors/failures.dart';
import 'package:sinarmas/core/feature/home/domain/list_meals.dart';
import 'package:sinarmas/core/feature/home/impl/home_repository.dart';

@injectable
class GetCategoriesListUseCase {
  final MealsRepository _repository;

  const GetCategoriesListUseCase(this._repository);

  Future<Either<Failure, ListCategoriesData>> execute() async {
    return _repository.getMealsList();
  }
}
