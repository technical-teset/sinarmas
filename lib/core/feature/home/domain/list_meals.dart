import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sinarmas/core/api/meals/response/get_meals.dart';
import 'package:sinarmas/core/feature/home/domain/meals.dart';

@immutable
class ListCategoriesData {
  final List<MealsData> meals;

  const ListCategoriesData({
    required this.meals,
  });

  factory ListCategoriesData.fromResponse(
    GetMealsListResponse? response,
  ) =>
      ListCategoriesData(
        meals: (response?.meals ?? []).map(MealsData.fromResponse).toList(),
      );
}
