import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sinarmas/core/api/meals/response/meals.dart';

@immutable
class MealsData {
  final String idMeal;
  final String strMeal;
  final String strCategory;
  final String strArea;
  final String strMealThumb;
  final String strInstructions;

  const MealsData({
    required this.idMeal,
    required this.strMeal,
    required this.strCategory,
    required this.strArea,
    required this.strMealThumb,
    required this.strInstructions,
  });

  factory MealsData.fromResponse(
    MealsResponse? response,
  ) =>
      MealsData(
        idMeal: response?.idMeal ?? '',
        strMeal: response?.strMeal ?? '',
        strCategory: response?.strCategory ?? '',
        strArea: response?.strArea ?? '',
        strMealThumb: response?.strMealThumb ?? '',
        strInstructions: response?.strInstructions ?? '',
      );
}
