import 'package:injectable/injectable.dart';
import 'package:sinarmas/core/api/meals/meals_api.dart';
import 'package:sinarmas/core/client/client.dart';

@module
abstract class RetrofitInjectableModule {
  MealsApi getMealsApi(ApiDio client) => MealsApi(client);
}
