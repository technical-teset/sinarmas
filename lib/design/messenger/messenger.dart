import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../molecules/molecules.dart';

class Messenger {
  static BuildContext? _buildContext;

  static BuildContext? get context => _buildContext;
  static set context(BuildContext? context) => _buildContext = context;

  static bool get hasContext => _buildContext != null;

  static void showSuccess(
    String message, {
    ActionType actionType = ActionType.normal,
    BuildContext? context,
    int durationInSeconds = 2,
  }) {
    _showToast(
      message: message,
      context: context,
      durationInSeconds: durationInSeconds,
      toastType: ToastType.success,
      actionType: actionType,
    );
  }

  static void _showToast({
    required String message,
    required ActionType actionType,
    BuildContext? context,
    ToastType toastType = ToastType.info,
    int durationInSeconds = 10,
    bool showLeading = true,
  }) {
    FToast()
      ..init(context ?? _buildContext!)
      ..removeCustomToast()
      ..showToast(
        toastDuration: Duration(seconds: durationInSeconds),
        positionedToastBuilder: (context, child) {
          return Positioned(
            top: 50,
            left: 24,
            right: 24,
            child: child,
          );
        },
        child: SMMToast(
          text: message,
          toastType: toastType,
          actionType: actionType,
          context: context ?? _buildContext!,
          showLeading: showLeading,
        ),
      );
  }
}
