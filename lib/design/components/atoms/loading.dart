import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:sinarmas/design/base/colors.dart';

enum SMLoadingType {
  waveDots,
  staggeredDotsWave,
}

enum SMLoadingColor {
  light(color: SMColors.smWhite),
  dark(color: SMColors.smTextLightPrimary);

  final Color color;
  const SMLoadingColor({
    required this.color,
  });
}

class SMALoading extends StatelessWidget {
  const SMALoading({
    this.loadingColor = SMLoadingColor.dark,
    this.type = SMLoadingType.waveDots,
    this.color,
    this.size = 48,
    Key? key,
  }) : super(key: key);

  final SMLoadingColor loadingColor;
  final Color? color;
  final double size;
  final SMLoadingType type;

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case SMLoadingType.waveDots:
        return Center(
          child: LoadingAnimationWidget.waveDots(
            color: color ?? loadingColor.color,
            size: size,
          ),
        );
      case SMLoadingType.staggeredDotsWave:
        return Center(
          child: LoadingAnimationWidget.staggeredDotsWave(
            color: color ?? loadingColor.color,
            size: size,
          ),
        );
    }
  }
}
