import 'package:flutter/material.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/components/atoms/atoms.dart';

enum SMButtonType {
  confirm(
    textColor: SMColors.smWhite,
    bgColor: SMColors.smPrimaryDark,
    borderColor: SMColors.smPrimaryDarker,
  ),
  cancel(
    textColor: SMColors.smTextLightPrimary,
    bgColor: SMColors.smErrorDark,
    borderColor: SMColors.smErrorDarker,
  );

  const SMButtonType({
    required this.textColor,
    required this.bgColor,
    required this.borderColor,
  });

  final Color textColor;
  final Color bgColor;
  final Color borderColor;
}

class BMAButton extends StatelessWidget {
  final SMButtonType type;
  final String? leadingIcon;
  final String text;
  final VoidCallback? onTap;

  const BMAButton({
    Key? key,
    this.type = SMButtonType.confirm,
    this.leadingIcon,
    required this.text,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: type.borderColor, width: 1),
          color: type.bgColor,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (leadingIcon != null) ...[
              Image.asset(
                leadingIcon!,
                height: 16,
                width: 16,
              ),
              const SizedBox(width: 4),
            ],
            SMAText.body2(
              text: text,
              color: type.textColor,
            ),
          ],
        ),
      ),
    );
  }
}
