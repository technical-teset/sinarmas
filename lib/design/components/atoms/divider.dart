import 'package:flutter/material.dart';

class SMADivider extends StatelessWidget {
  const SMADivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Divider(),
    );
  }
}
