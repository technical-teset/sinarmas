import 'package:flutter/material.dart';
import 'package:sinarmas/design/components/atoms/atoms.dart';

class SMMErrorMessage extends StatelessWidget {
  const SMMErrorMessage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.error, size: 128),
          SizedBox(height: 24),
          SMAText.h6(text: 'Maaf, silahkan di refresh kembali!'),
        ],
      ),
    );
  }
}
