import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sinarmas/design/base/colors.dart';

import '../components/atoms/atoms.dart';

enum ToastType { success, info, error, warning }

enum ActionType { normal }

class SMMToast extends StatelessWidget {
  final String text;
  final ToastType toastType;
  final ActionType actionType;
  final BuildContext context;
  final bool showLeading;

  const SMMToast({
    Key? key,
    required this.text,
    required this.toastType,
    required this.actionType,
    required this.context,
    required this.showLeading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: SMColors.bgPaperDark,
        borderRadius: BorderRadius.circular(50),
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 18,
        vertical: 12,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          if (showLeading) _mapLeading(toastType),
          Flexible(
            child: SMAText.subtitle2(
              text: text,
              overflow: TextOverflow.visible,
              color: SMColors.smWhite,
              maxLines: 2,
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }

  Widget _mapLeading(ToastType snackType) {
    switch (snackType) {
      case ToastType.info:
        return _leadingWidget(
          const Icon(
            Icons.info,
            size: 24,
            color: SMColors.smInfo,
          ),
        );
      case ToastType.success:
        return _leadingWidget(
          const Icon(
            Icons.check_circle,
            size: 24,
            color: SMColors.smSuccess,
          ),
        );
      case ToastType.warning:
        return _leadingWidget(
          const Icon(
            Icons.error,
            size: 24,
            color: SMColors.smWarning,
          ),
        );
      case ToastType.error:
        return _leadingWidget(
          const Icon(
            Icons.dangerous,
            size: 24,
            color: SMColors.smError,
          ),
        );
    }
  }

  Widget _leadingWidget(Icon icon) {
    return Row(
      children: [
        icon,
        const SizedBox(width: 12),
      ],
    );
  }
}
