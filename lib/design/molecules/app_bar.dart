import 'package:flutter/material.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/components/atoms/atoms.dart';
import 'package:sinarmas/route/go_router/router.dart';

class SMMAppBar extends StatelessWidget {
  final String title;
  final VoidCallback? onTap;

  const SMMAppBar({
    Key? key,
    required this.title,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: SMColors.smDivider,
          ),
        ),
      ),
      child: Row(
        children: [
          GestureDetector(
            onTap: onTap ?? goRouter.pop,
            child: const Icon(
              Icons.arrow_back,
              size: 24,
            ),
          ),
          const SizedBox(width: 16),
          SMAText.h5(text: title),
        ],
      ),
    );
  }
}
