import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sinarmas/design/components/atoms/sizing_information_builder.dart';
import 'package:sinarmas/design/molecules/scaffold.dart';
import 'package:sinarmas/design/templates/base_state_normal.dart';
import 'package:sinarmas/utils/helper/helper.dart';

abstract class BaseStateful<T extends StatefulWidget> extends State<T>
    with Diagnosticable
    implements BaseStateNormal {
  late GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  @override
  void initState() {
    super.initState();
    init();
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  }

  @override
  void dispose() {
    refreshIndicatorKey.currentState?.dispose();
    super.dispose();
  }

  ScaffoldAttribute buildAttribute();

  @override
  Widget build(BuildContext context) {
    final scaffoldAttribute = buildAttribute();
    Widget child = SizingInformationBuilder(
      builder: (_, sizeInfo) {
        if (sizeInfo.deviceType == DeviceScreenType.mobile) {
          return buildNarrowLayout(
            context,
            sizeInfo,
          );
        }
        return buildWideLayout(
          context,
          sizeInfo,
        );
      },
    );
    // should check whether scaffold need to remove or not
    if (!scaffoldAttribute.removeScaffold) {
      child = MTScaffold(
        attr: scaffoldAttribute,
        body: child,
        appBar: buildAppBar(context),
      );
    }
    if (scaffoldAttribute.backgroundImageAsset?.isNotEmpty ?? false) {
      child = Stack(
        children: [
          SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    scaffoldAttribute.backgroundImageAsset!,
                    package: scaffoldAttribute.backgroundImagePackage,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          child,
        ],
      );
    }
    return GestureDetector(
      onTap: () {
        if (scaffoldAttribute.autoClearFocus) clearFocus(context);
      },
      child: WillPopScope(
        onWillPop: onBackPressed,
        child: child,
      ),
    );
  }
}
