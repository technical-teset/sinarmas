import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void clearFocus(BuildContext context) {
  final currentFocus = FocusScope.of(context);
  if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
}

void clearFormText(BuildContext context) {
  FocusScope.of(context).requestFocus(FocusNode());
  SystemChannels.textInput.invokeMethod('TextInput.hide');
}
