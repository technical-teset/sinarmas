import 'package:flutter/material.dart';
import 'package:sinarmas/design/components/atoms/atoms.dart';

class EmptyWidget extends StatelessWidget {
  const EmptyWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Column(
        children: [
          Icon(Icons.question_mark_outlined, size: 64),
          SizedBox(height: 12),
          SMAText.subtitle2(text: 'Ooops!'),
          SizedBox(height: 2),
          SMAText.body2(
            overflow: TextOverflow.visible,
            text:
                'Silahkan tambahkan makanan melalui\nmenu food list terlebih dahulu',
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
