import 'package:sinarmas/database/local/db/app_db.dart';

mixin FavoriteScreenContract {
  void getFavorite({required MyDatabase myDb});
}
