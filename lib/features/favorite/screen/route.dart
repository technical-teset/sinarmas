// Flutter imports:
// Package imports:
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:sinarmas/di/core_di.dart';
import 'package:sinarmas/features/favorite/blocs/favorite_bloc.dart';
import 'package:sinarmas/features/favorite/screen/ui.dart';
import 'package:sinarmas/route/app_routes.dart';
import 'package:sinarmas/route/go_router/router.dart';

part 'route.g.dart';

@TypedGoRoute<FavoriteRoute>(
  path: AppRoutes.favoriteScreen,
)
class FavoriteRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) {
    return BlocProvider(
      create: (context) => getIt<FavoriteBloc>(),
      child: const FavoriteScreen(),
    );
  }

  static final GlobalKey<NavigatorState> $parentNavigatorKey = rootNavigatorKey;
}
