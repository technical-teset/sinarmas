import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sinarmas/database/local/db/app_db.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/components/atoms/sizing_information_builder.dart';
import 'package:sinarmas/design/molecules/molecules.dart';
import 'package:sinarmas/design/molecules/scaffold.dart';
import 'package:sinarmas/design/templates/base_stateful.dart';
import 'package:sinarmas/features/detail/screen/route.dart';
import 'package:sinarmas/features/favorite/blocs/favorite_bloc.dart';
import 'package:sinarmas/features/favorite/screen/contract.dart';
import 'package:drift/drift.dart' as drift;
import 'package:sinarmas/features/favorite/widget/empty_widget.dart';
import 'package:sinarmas/route/go_router/router.dart';

import '../../../design/components/atoms/atoms.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends BaseStateful<FavoriteScreen>
    with FavoriteScreenContract {
  FavoriteBloc get _viewModel => context.read<FavoriteBloc>();
  late MyDatabase _myDb;

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return const PreferredSize(
      preferredSize: Size.fromHeight(100),
      child: SafeArea(
        child: SMMAppBar(title: 'Favorite List'),
      ),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute(backgroundColor: SMColors.bgNeutralLight);
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return SafeArea(
      child: RefreshIndicator(
        onRefresh: onRefresh,
        child: ListView(
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.all(24),
              child: BlocBuilder<FavoriteBloc, FavoriteState>(
                builder: (context, state) {
                  switch (state.pageStatus) {
                    case FavoriteStatus.initial:
                      return const SMALoading();
                    case FavoriteStatus.loading:
                      return const SMALoading();
                    case FavoriteStatus.failed:
                      return const SMMErrorMessage();
                    case FavoriteStatus.loaded:
                      final isEmpty = state.isEmpty;
                      switch (isEmpty) {
                        case true:
                          return const EmptyWidget();
                        case false:
                          return ListView.separated(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: state.listFavorite.length,
                            itemBuilder: (context, index) {
                              final item = state.listFavorite[index];
                              return SMAContainer(
                                onTap: () async {
                                  final route =
                                      DetailRoute(mealsId: item.mealsId);
                                  await goRouter.push(route.location,
                                      extra: route);
                                  onRefresh.call();
                                },
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            flex: 2,
                                            child: Image.network(
                                                item.mealsThumbnail),
                                          ),
                                          const SizedBox(width: 12),
                                          Expanded(
                                            flex: 3,
                                            child: Column(
                                              children: [
                                                SMAText.h6(
                                                    text: item.mealsName),
                                                const SizedBox(height: 2),
                                                SMAText.body2(
                                                  text:
                                                      '${item.mealsCategory} - ${item.mealsArea}',
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const Icon(Icons.chevron_right, size: 24),
                                  ],
                                ),
                              );
                            },
                            separatorBuilder: (context, index) =>
                                const SizedBox(height: 12),
                          );
                      }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  void init() {
    _myDb = MyDatabase();
    getFavorite.call(myDb: _myDb);
  }

  @override
  void dispose() {
    _myDb.close();
    _viewModel.close();
    super.dispose();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  @override
  void getFavorite({required MyDatabase myDb}) {
    _viewModel.add(FavoriteGetDataEvent(myDb: myDb));
  }

  Future<void> onRefresh() async {
    getFavorite.call(myDb: _myDb);
  }
}
