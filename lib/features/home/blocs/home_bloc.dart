import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:sinarmas/base/initializer/initializer.dart';
import 'package:sinarmas/core/feature/home/domain/meals.dart';
import 'package:sinarmas/core/feature/home/usecase/get_meals_list.dart';

part 'home_event.dart';
part 'home_state.dart';

@singleton
class HomeBloc extends Bloc<HomeEvent, HomeState> with CacheKey {
  HomeBloc() : super(HomeState.createDefault());

  @override
  String get key => 'home-bloc';
}
