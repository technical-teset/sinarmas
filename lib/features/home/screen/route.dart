// Flutter imports:
// Package imports:
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:sinarmas/features/home/screen/ui.dart';
import 'package:sinarmas/route/app_routes.dart';
import 'package:sinarmas/route/go_router/router.dart';

part 'route.g.dart';

@TypedGoRoute<HomeRoute>(
  path: AppRoutes.homeScreen,
)
class HomeRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const HomeScreen();
  }

  static final GlobalKey<NavigatorState> $parentNavigatorKey = rootNavigatorKey;
}
