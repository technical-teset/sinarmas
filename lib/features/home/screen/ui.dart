import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/components/atoms/atoms.dart';
import 'package:sinarmas/design/components/atoms/container.dart';
import 'package:sinarmas/design/components/atoms/sizing_information_builder.dart';
import 'package:sinarmas/design/molecules/scaffold.dart';
import 'package:sinarmas/design/templates/base_stateful.dart';
import 'package:sinarmas/di/core_di.dart';
import 'package:sinarmas/features/favorite/screen/route.dart';
import 'package:sinarmas/features/food_list/screen/route.dart';
import 'package:sinarmas/features/home/blocs/home_bloc.dart';
import 'package:sinarmas/features/home/screen/contract.dart';
import 'package:sinarmas/route/go_router/router.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends BaseStateful<HomeScreen>
    with HomeScreenContract {
  HomeBloc get _viewModel => getIt<HomeBloc>();

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return null;
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute(backgroundColor: SMColors.bgNeutralLight);
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return SafeArea(
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.all(24),
            child: ListView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                const SMAText.h4(text: 'Welcome!'),
                const SizedBox(height: 24),
                const SMAText.h5(text: 'Please choose one menu'),
                const SizedBox(height: 16),
                InkWell(
                  onTap: () {
                    final route = FoodListRoute();
                    goRouter.push(route.location, extra: route);
                  },
                  child: const SMAContainer(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.fastfood_rounded, size: 36),
                        SizedBox(width: 12),
                        SMAText.subtitle1(text: 'Food List'),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 24),
                SMAContainer(
                  onTap: () {
                    final route = FavoriteRoute();
                    goRouter.push(route.location, extra: route);
                  },
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.favorite, size: 36),
                      SizedBox(width: 12),
                      SMAText.subtitle1(text: 'Favorites'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  void init() {}

  @override
  Future<bool> onBackPressed() async {
    return false;
  }
}
