// Flutter imports:
// Package imports:
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:sinarmas/di/core_di.dart';
import 'package:sinarmas/features/food_list/blocs/food_list_bloc.dart';
import 'package:sinarmas/features/food_list/screen/ui.dart';
import 'package:sinarmas/features/home/blocs/home_bloc.dart';
import 'package:sinarmas/features/home/screen/ui.dart';
import 'package:sinarmas/route/app_routes.dart';
import 'package:sinarmas/route/go_router/router.dart';

part 'route.g.dart';

@TypedGoRoute<FoodListRoute>(
  path: AppRoutes.foodListScreen,
)
class FoodListRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) {
    return BlocProvider(
      create: (context) => getIt<FoodListBloc>(),
      child: const FoodListScreen(),
    );
  }

  static final GlobalKey<NavigatorState> $parentNavigatorKey = rootNavigatorKey;
}
