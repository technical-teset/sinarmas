import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/components/atoms/atoms.dart';
import 'package:sinarmas/design/components/atoms/container.dart';
import 'package:sinarmas/design/components/atoms/sizing_information_builder.dart';
import 'package:sinarmas/design/molecules/molecules.dart';
import 'package:sinarmas/design/molecules/scaffold.dart';
import 'package:sinarmas/design/templates/base_stateful.dart';
import 'package:sinarmas/features/detail/screen/route.dart';
import 'package:sinarmas/features/food_list/blocs/food_list_bloc.dart';
import 'package:sinarmas/features/food_list/screen/contract.dart';
import 'package:sinarmas/route/go_router/router.dart';

class FoodListScreen extends StatefulWidget {
  const FoodListScreen({Key? key}) : super(key: key);

  @override
  State<FoodListScreen> createState() => _FoodListScreenState();
}

class _FoodListScreenState extends BaseStateful<FoodListScreen>
    with FoodListScreenContract {
  FoodListBloc get _viewModel => context.read<FoodListBloc>();

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return const PreferredSize(
      preferredSize: Size.fromHeight(100),
      child: SafeArea(
        child: SMMAppBar(title: 'Food List'),
      ),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute(backgroundColor: SMColors.bgNeutralLight);
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return SafeArea(
      child: RefreshIndicator(
        onRefresh: onRefresh,
        child: ListView(
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.all(24),
              child: BlocBuilder<FoodListBloc, FoodListState>(
                builder: (context, state) {
                  switch (state.pageStatus) {
                    case FoodListStatus.initial:
                      return const SMALoading();
                    case FoodListStatus.loading:
                      return const SMALoading();
                    case FoodListStatus.failed:
                      return const SMMErrorMessage();
                    case FoodListStatus.loaded:
                      return ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: state.mealsList.length,
                        itemBuilder: (context, index) {
                          final item = state.mealsList[index];
                          return SMAContainer(
                            onTap: () {
                              final route = DetailRoute(mealsId: item.idMeal);
                              goRouter.push(route.location, extra: route);
                            },
                            child: Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Image.network(item.strMealThumb),
                                      ),
                                      const SizedBox(width: 12),
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          children: [
                                            SMAText.h6(text: item.strMeal),
                                            const SizedBox(height: 2),
                                            SMAText.body2(
                                              text:
                                                  '${item.strCategory} - ${item.strArea}',
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const Icon(Icons.chevron_right, size: 24),
                              ],
                            ),
                          );
                        },
                        separatorBuilder: (context, index) =>
                            const SizedBox(height: 12),
                      );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  void init() {
    getData.call();
  }

  @override
  void dispose() {
    _viewModel.close();
    super.dispose();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  Future<void> onRefresh() async {
    getData.call();
  }

  @override
  getData() {
    _viewModel.add(FoodListGetDataEvent());
  }
}
