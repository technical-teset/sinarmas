import 'package:flutter/material.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/base/texts.dart';

class SMBottomNavigation extends StatelessWidget {
  final int currentIndex;
  final ValueSetter<int> onTap;

  const SMBottomNavigation({
    Key? key,
    required this.currentIndex,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: SMColors.smBlack,
        border: Border(
          top: BorderSide(
            color: SMColors.smGrey24,
          ),
        ),
        boxShadow: [
          BoxShadow(
            color: SMColors.smGrey24,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: onTap,
        backgroundColor: SMColors.smBlack,
        selectedItemColor: SMColors.smPrimary,
        selectedLabelStyle: body1,
        showSelectedLabels: true,
        unselectedItemColor: SMColors.smTextLightDisabled,
        unselectedLabelStyle: body1,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        items: const [
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.fromLTRB(8, 0, 8, 4),
              child: Icon(Icons.home),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.fromLTRB(8, 0, 8, 4),
              child: Icon(Icons.star),
            ),
            label: 'Transaksi',
          ),
        ],
      ),
    );
  }
}
