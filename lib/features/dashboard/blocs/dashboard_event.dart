part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardEvent {}

class DashboardChangeCurrentIndexEvent extends DashboardEvent {
  final int index;
  DashboardChangeCurrentIndexEvent({required this.index});
}
