import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

@injectable
class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  DashboardBloc() : super(DashboardState.createDefault()) {
    on<DashboardChangeCurrentIndexEvent>(_changeCurrentIndex);
  }

  Future<void> _changeCurrentIndex(
    DashboardChangeCurrentIndexEvent event,
    Emitter<DashboardState> emit,
  ) async {
    emit(state.copyWith(currentIndex: event.index));
  }
}
