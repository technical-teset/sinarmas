part of 'dashboard_bloc.dart';

class DashboardState extends Equatable {
  final int currentIndex;

  const DashboardState({
    this.currentIndex = 0,
  });

  factory DashboardState.createDefault() => const DashboardState();

  DashboardState copyWith({
    int? currentIndex,
  }) =>
      DashboardState(
        currentIndex: currentIndex ?? this.currentIndex,
      );

  @override
  List<Object?> get props => [
        currentIndex,
      ];
}
