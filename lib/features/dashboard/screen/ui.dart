import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/templates/base_stateful.dart';
import 'package:sinarmas/features/dashboard/screen/contract.dart';
import 'package:sinarmas/features/dashboard/widget/dashboard_widget.dart';
import 'package:sinarmas/route/app_routes.dart';
import 'package:sinarmas/route/go_router/router.dart';

import '../../../design/components/atoms/atoms.dart';
import '../../../design/molecules/molecules.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends BaseStateful<DashboardScreen>
    with DashboardContract {
  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return null;
  }

  @override
  ScaffoldAttribute buildAttribute() {
    final raw = goRouter.routerDelegate.currentConfiguration.uri.toString();
    String path = '';
    if (raw.isNotEmpty) {
      path = raw;
    }
    final shouldHide = !AppRoutes.dashboardRoutes.contains(path);

    final Widget child = AnimatedSlide(
      duration: const Duration(milliseconds: 350),
      curve: Curves.easeInOut,
      offset: shouldHide ? const Offset(0, 1) : Offset.zero,
      onEnd: () {},
      child: SMBottomNavigation(
        currentIndex: _calculateSelectedIndex(context),
        onTap: (index) => onTap(index),
      ),
    );
    return ScaffoldAttribute(
      backgroundColor: SMColors.smTransparent,
      bottomNavigation: child,
      isResizable: false,
    );
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return widget.child;
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  void init() {}

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  int _calculateSelectedIndex(BuildContext context) {
    final String location =
        goRouter.routerDelegate.currentConfiguration.uri.toString();
    if (location.startsWith(AppRoutes.homeScreen)) {
      return 0;
    } else if (location.startsWith(AppRoutes.favoriteScreen)) {
      return 1;
    }
    return 0;
  }

  @override
  void onTap(int value) {
    switch (value) {
      case 0:
        return goRouter.go(AppRoutes.homeScreen);
      case 1:
        return goRouter.go(AppRoutes.favoriteScreen);
      default:
        return goRouter.go(AppRoutes.homeScreen);
    }
  }
}
