import 'package:flutter/material.dart';
import 'package:sinarmas/database/local/db/app_db.dart';

mixin DetailScreenContract {
  void getDetail({required MyDatabase myDb});
  void toggleFavorite({required BuildContext context});
}
