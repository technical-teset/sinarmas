import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sinarmas/database/local/db/app_db.dart';
import 'package:sinarmas/database/local/entity/favorite_entity.dart';
import 'package:sinarmas/design/base/colors.dart';
import 'package:sinarmas/design/components/atoms/atoms.dart';
import 'package:sinarmas/design/components/atoms/sizing_information_builder.dart';
import 'package:sinarmas/design/molecules/molecules.dart';
import 'package:sinarmas/design/molecules/scaffold.dart';
import 'package:sinarmas/design/templates/base_stateful.dart';
import 'package:sinarmas/features/detail/blocs/detail_bloc.dart';
import 'package:sinarmas/features/detail/screen/contract.dart';
import 'package:drift/drift.dart' as drift;

class DetailScreen extends StatefulWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends BaseStateful<DetailScreen>
    with DetailScreenContract {
  DetailBloc get _viewModel => context.read<DetailBloc>();
  late MyDatabase _myDb;

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return const PreferredSize(
      preferredSize: Size.fromHeight(100),
      child: SafeArea(
        child: SMMAppBar(title: 'Detail'),
      ),
    );
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return SafeArea(
      child: RefreshIndicator(
        onRefresh: onRefresh,
        child: ListView(
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.all(24),
              child: BlocBuilder<DetailBloc, DetailState>(
                builder: (context, state) {
                  switch (state.pageStatus) {
                    case DetailStatus.initial:
                      return const SMALoading();
                    case DetailStatus.loading:
                      return const SMALoading();
                    case DetailStatus.failed:
                      return const SMMErrorMessage();
                    case DetailStatus.loaded:
                      final item = state.mealDetail[0];
                      return SMAContainer(
                        child: ListView(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SMAText.h5(
                                    text:
                                        '${item.strMeal} (${item.strArea} Food)'),
                                InkWell(
                                  onTap: () => addOrRemoveFavorite(state),
                                  child: CircleAvatar(
                                    backgroundColor: SMColors.smTransparent,
                                    child: state.isFavorite
                                        ? const Icon(
                                            Icons.favorite,
                                            color: Colors.red,
                                          )
                                        : const Icon(
                                            Icons.favorite_border,
                                            color: Colors.black,
                                          ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 12),
                            Image.network(item.strMealThumb),
                            const SizedBox(height: 24),
                            const SMAText.subtitle1(text: 'Category:'),
                            const SizedBox(height: 4),
                            SMAText.body1(text: item.strCategory),
                            const SizedBox(height: 24),
                            const SMAText.subtitle1(text: 'Description:'),
                            const SizedBox(height: 4),
                            SMAText.body1(
                              text: item.strInstructions,
                              overflow: TextOverflow.visible,
                            ),
                          ],
                        ),
                      );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return buildNarrowLayout(context, sizeInfo);
  }

  @override
  void init() {
    _myDb = MyDatabase();
    getDetail.call(myDb: _myDb);
  }

  @override
  void dispose() {
    _myDb.close();
    _viewModel.close();
    super.dispose();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  Future<void> onRefresh() async {
    getDetail.call(myDb: _myDb);
  }

  @override
  void getDetail({required MyDatabase myDb}) {
    _viewModel.add(DetailGetDataEvent(myDb: myDb));
  }

  @override
  void toggleFavorite({required BuildContext context}) {
    _viewModel.add(DetailToggleFavoriteEvent(context: context));
  }

  void addOrRemoveFavorite(DetailState state) {
    switch (state.isFavorite) {
      case false:
        addFavorite(state);
        break;
      case true:
        removeFavorite(state);
        break;
    }
  }

  Future<void> addFavorite(DetailState state) async {
    final item = state.mealDetail[0];
    final entity = FavoriteCompanion(
      mealsId: drift.Value(item.idMeal),
      mealsArea: drift.Value(item.strArea),
      mealsName: drift.Value(item.strMeal),
      mealsThumbnail: drift.Value(item.strMealThumb),
      mealsInstruction: drift.Value(item.strInstructions),
      mealsCategory: drift.Value(item.strCategory),
    );

    await _myDb.addFavorite(entity).then((value) {
      toggleFavorite.call(context: context);
    });
  }

  Future<void> removeFavorite(DetailState state) async {
    final item = state.mealDetail[0].idMeal;
    await _myDb.removeFavorite(item).then((value) {
      toggleFavorite.call(context: context);
    });
  }
}
