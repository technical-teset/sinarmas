// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:sinarmas/core/api/meals/meals_api.dart' as _i7;
import 'package:sinarmas/core/api/retrofit_module.dart' as _i14;
import 'package:sinarmas/core/client/api_dio.dart' as _i3;
import 'package:sinarmas/core/client/client.dart' as _i8;
import 'package:sinarmas/core/feature/home/impl/home_repository.dart' as _i9;
import 'package:sinarmas/core/feature/home/usecase/get_meals_detail.dart'
    as _i11;
import 'package:sinarmas/core/feature/home/usecase/get_meals_list.dart' as _i10;
import 'package:sinarmas/features/dashboard/blocs/dashboard_bloc.dart' as _i4;
import 'package:sinarmas/features/detail/blocs/detail_bloc.dart' as _i12;
import 'package:sinarmas/features/favorite/blocs/favorite_bloc.dart' as _i5;
import 'package:sinarmas/features/food_list/blocs/food_list_bloc.dart' as _i13;
import 'package:sinarmas/features/home/blocs/home_bloc.dart' as _i6;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt $initCoreGetIt({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final retrofitInjectableModule = _$RetrofitInjectableModule();
    gh.factory<_i3.ApiDio>(() => _i3.ApiDio());
    gh.factory<_i4.DashboardBloc>(() => _i4.DashboardBloc());
    gh.singleton<_i5.FavoriteBloc>(_i5.FavoriteBloc());
    gh.singleton<_i6.HomeBloc>(_i6.HomeBloc());
    gh.factory<_i7.MealsApi>(
        () => retrofitInjectableModule.getMealsApi(gh<_i8.ApiDio>()));
    gh.factory<_i9.MealsRepository>(
        () => _i9.MealsRepository(api: gh<_i7.MealsApi>()));
    gh.factory<_i10.GetCategoriesListUseCase>(
        () => _i10.GetCategoriesListUseCase(gh<_i9.MealsRepository>()));
    gh.factory<_i11.GetDetailUseCase>(
        () => _i11.GetDetailUseCase(gh<_i9.MealsRepository>()));
    gh.factoryParam<_i12.DetailBloc, _i12.DetailState?, dynamic>((
      initialValue,
      _,
    ) =>
        _i12.DetailBloc(
          gh<_i11.GetDetailUseCase>(),
          initialValue: initialValue,
        ));
    gh.factory<_i13.FoodListBloc>(
        () => _i13.FoodListBloc(gh<_i10.GetCategoriesListUseCase>()));
    return this;
  }
}

class _$RetrofitInjectableModule extends _i14.RetrofitInjectableModule {}
