import 'package:shared_preferences/shared_preferences.dart';

part 'cache_key.dart';
part 'delay.dart';

class Initializer {
  String ck;
  bool state;
  void Function() whenTrigger;
  InitializerDelay delay;

  Initializer({
    required String cacheKey,
    required this.state,
    required this.whenTrigger,
    this.delay = InitializerDelay.sec15,
  })  : assert(cacheKey != '', ''),
        ck = cacheKey;

  void initialize() {
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(ck)) {
        final date = value.getString(ck);
        if (_getDelayTime(delay, date) && state) {
        } else {
          await value.setString(ck, DateTime.now().toIso8601String());
          whenTrigger.call();
        }
      } else {
        await value.setString(ck, DateTime.now().toIso8601String());
        whenTrigger.call();
      }
    });
  }
}

bool _getDelayTime(InitializerDelay delay, String? date) {
  if (date == null) return false;
  var dur = const Duration(minutes: 15);
  if (delay == InitializerDelay.min1) {
    dur = const Duration(minutes: 1);
  } else if (delay == InitializerDelay.min5) {
    dur = const Duration(minutes: 5);
  } else if (delay == InitializerDelay.sec15) {
    dur = const Duration(seconds: 15);
  }

  return isDifferenceLessThan(date, dur);
}

bool isDifferenceLessThan(String time, Duration duration) {
  return DateTime.now().difference(DateTime.parse(time)) < duration;
}
