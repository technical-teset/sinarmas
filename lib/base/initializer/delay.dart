part of 'initializer.dart';

enum InitializerDelay {
  min1,
  min5,
  sec15,
}
