// Flutter imports:
// Package imports:
// ignore_for_file: library_prefixes

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:sinarmas/route/app_routes.dart';

//Routes
import 'package:sinarmas/features/home/screen/route.dart' as homeRoute;
import 'package:sinarmas/features/favorite/screen/route.dart' as favoriteRoute;
import 'package:sinarmas/features/food_list/screen/route.dart' as foodListRoute;
import 'package:sinarmas/features/detail/screen/route.dart' as detailRoute;

// Project imports:

final GlobalKey<NavigatorState> rootNavigatorKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> _shellNavigatorKey =
    GlobalKey<NavigatorState>();

GoRouter goRouter = GoRouter(
  navigatorKey: rootNavigatorKey,
  initialLocation: AppRoutes.homeScreen,
  routes: [
    // ShellRoute(
    //   navigatorKey: rootNavigatorKey,
    //   observers: [
    //     GoRouterNavigationObserver(),
    //   ],
    //   builder: (context, state, child) {
    //     return MultiBlocProvider(
    //       providers: [
    //         BlocProvider<HomeBloc>(
    //           create: (context) => getIt<HomeBloc>(),
    //         ),
    //         BlocProvider<FavoriteBloc>(
    //           create: (context) => getIt<FavoriteBloc>(),
    //         ),
    //       ],
    //       child: DashboardScreen(child: child),
    //     );
    //   },
    //   routes: <RouteBase>[
    //     ...homeRoute.$appRoutes,
    //     ...favoriteRoute.$appRoutes,
    //   ],
    // ),

    ...homeRoute.$appRoutes,
    ...favoriteRoute.$appRoutes,
    ...foodListRoute.$appRoutes,
    ...detailRoute.$appRoutes,
  ],
  observers: <NavigatorObserver>[
    GoRouterNavigationObserver(),
  ],
);

class GoRouterNavigationObserver extends NavigatorObserver {
  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    _updateRoute();
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    _updateRoute();
  }

  @override
  void didRemove(Route<dynamic> route, Route<dynamic>? previousRoute) {
    _updateRoute();
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    _updateRoute();
  }

  void _updateRoute() {
    print('update route');
  }
}
