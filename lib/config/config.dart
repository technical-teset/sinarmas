import 'package:sinarmas/di/core_di.dart';

class Config {
  /// Initialize Config.
  static Future<void> init() async {
    await configureCoreDependencies();
  }
}
