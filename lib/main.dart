import 'package:flutter/material.dart';
import 'package:sinarmas/config/config.dart';
import 'package:sinarmas/route/go_router/router.dart';

Future<void> main() async {
  await Config.init();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const SinarmasApp());
}

class SinarmasApp extends StatefulWidget {
  const SinarmasApp({Key? key}) : super(key: key);

  @override
  State<SinarmasApp> createState() => _SinarmasAppState();
}

class _SinarmasAppState extends State<SinarmasApp> {
  final UniqueKey _key = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      key: _key,
      title: 'Sinarmas Food',
      routerConfig: goRouter,
      builder: (context, child) {
        final MediaQueryData data = MediaQuery.of(context);
        double textScaleFactor = 1;
        if (data.size.height < 600) {
          textScaleFactor = 0.75;
        }
        final screenHost = Overlay(
          initialEntries: [
            if (child != null) ...[
              OverlayEntry(
                builder: (context) => _ScreenHost(child: child),
              ),
            ],
          ],
        );
        return MediaQuery(
          data: data.copyWith(textScaleFactor: textScaleFactor),
          child: screenHost,
        );
      },
    );
  }
}

class _ScreenHost extends StatelessWidget {
  const _ScreenHost({
    this.child,
  });
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: child ?? const SizedBox.shrink(),
    );
  }
}
